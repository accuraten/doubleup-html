module.exports =
  dev:
    options:
      map: false
      processors: [
        require('autoprefixer-core')(browsers: 'last 2 versions')
      ]
    files: [ {
      expand: true
      cwd: 'public/css'
      src: [ '*.css' ]
      dest: 'public/css'
      ext: '.css'
    } ]
  prod:
    options:
      map: false
      processors: [
        require('autoprefixer-core')(browsers: 'last 2 versions')
        require('csswring')
      ]
    files: [ {
      expand: true
      cwd: 'public/css'
      src: [ '*.css' ]
      dest: 'public/css'
      ext: '.css'
    } ]
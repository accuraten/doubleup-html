module.exports =
  icons:
    src: 'src/svg-icons/*.svg'
    dest: 'public/fonts'
    types: 'eot,woff,ttf'
    options:
      syntax: 'bootstrap'
      templateOptions:
        baseClass: 'ka-icon'
        classPrefix: 'ka-'
        mixinPrefix: 'ka-'